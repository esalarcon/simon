#ifndef PROJECTS_FREERTOS_SIMON_INC_JUGARSIMON_H_
#define PROJECTS_FREERTOS_SIMON_INC_JUGARSIMON_H_
#define	PASOS_SIMON	16
#define GANO_SIMON	1
#define PERDIO_SIMON 2

typedef enum
{
	AZUL = 0, AMARILLO, ROJO, VERDE, APAGADO
} coloresSimon;

typedef enum
{
	S_AZUL = 410, S_AMARILLO = 310, S_ROJO = 252, S_VERDE = 209, S_PERDIO = 42
} sonidosSimon;

typedef struct
{
	unsigned char secuencia[16];
	int contador;
	int ultimocolor;
	int espera;
	int estadoMostrarSecuencia;
	int estadoEsperarBotones;
	int estadoJuego;
	int estadoPerdio;
	int estadoGano;
	int ledmostrados;
	int pasoJugador;
} datosSimon;

void iniciarSimon(datosSimon *datos);
int jugarSimon(datosSimon *datos, int *subidas, int *bajadas);
int generarRandom(void);

#endif /* PROJECTS_FREERTOS_SIMON_INC_JUGARSIMON_H_ */
