#include "../../freertos_simon/inc/main.h"

#include "../../freertos_simon/inc/debounce.h"
#include "../../freertos_simon/inc/FreeRTOSConfig.h"
#include "../../freertos_simon/inc/jugarsimon.h"
#include "../../freertos_simon/inc/pulsadores.h"
#include "../../freertos_simon/inc/pwm.h"
#include "board.h"
#include "FreeRTOS.h"
#include "task.h"

static void initHardware(void)
{
	SystemCoreClockUpdate();
	Board_Init();
	InitPulsadores();
	InitPWM();
}

static void task(void * a)
{
	int i;
	int jugando = 0;
	int iniciar = 0;
	datosSimon datosJuego;
	debounce_data debounce[NPULSADORES];
	int flancosSubidas[NPULSADORES];
	int flancosBajadas[NPULSADORES];

	iniciarSimon(&datosJuego);
	for (i = 0; i < NPULSADORES; i++)
		debounce_init(&debounce[i], ACTIVO_BAJO);

	while (1)
	{
		iniciar = 0;
		for (i = 0; i < NPULSADORES; i++)
		{
			iniciar |= is_pin_activo(&debounce[i], LeerPulsador(i));
			flancosBajadas[i] = debounce[i].flancoBajada;
			flancosSubidas[i] = debounce[i].flancoSubida;
			debounce[i].debtime--;
		}
		generarRandom();
		if (jugando || iniciar)
		{
			jugando = jugarSimon(&datosJuego, flancosSubidas, flancosBajadas);
		}
		datosJuego.espera--;
		vTaskDelay(10 / portTICK_RATE_MS);
	}
}

int main(void)
{
	initHardware();
	xTaskCreate(task, (const char * )"task", configMINIMAL_STACK_SIZE*8, 0,
			tskIDLE_PRIORITY+1, 0);
	vTaskStartScheduler();
	for (;;)
		;
}
