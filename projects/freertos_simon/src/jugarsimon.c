#include "jugarsimon.h"
#include "pulsadores.h"
#include "board.h"
#include "pwm.h"

int getTiempoColor(int contador, int gano)
{
	//Tomando una base de tiempo de 10ms.
	int ret;
	if (gano)
		return 10;
	if (contador < 5)
		ret = 40;
	if (contador >= 5 && contador < 13)
		ret = 30;
	if (contador >= 13)
		ret = 20;
	return ret;
}

int getTiempoEntreColores(int contador)
{
	//Tomando una base de tiempo de 10ms.
	return 5;
}

int getTiempoEsperaBoton(void)
{
	return 4500;
}

int getTiempoPerdio(void)
{
	return 150;
}

void mostrarColor(coloresSimon color)
{
	if (color == AZUL)
		Board_LED_Set(2, true);
	if (color == AMARILLO)
		Board_LED_Set(3, true);
	if (color == ROJO)
		Board_LED_Set(4, true);
	if (color == VERDE)
		Board_LED_Set(5, true);
	if (color == APAGADO)
	{
		Board_LED_Set(2, false);
		Board_LED_Set(3, false);
		Board_LED_Set(4, false);
		Board_LED_Set(5, false);
	}
}

int getFreqColor(coloresSimon color)
{
	int ret = 0;
	if (color == AZUL)
		ret = S_AZUL;
	if (color == AMARILLO)
		ret = S_AMARILLO;
	if (color == ROJO)
		ret = S_ROJO;
	if (color == VERDE)
		ret = S_VERDE;
	return ret;
}

void iniciarSimon(datosSimon *datos)
{
	int i;
	for (i = 0; i < PASOS_SIMON; i++)
	{
		datos->secuencia[i] = generarRandom();
	}
	datos->contador = 0;
	datos->espera = 0;
	datos->estadoEsperarBotones = 0;
	datos->estadoGano = 0;
	datos->estadoJuego = 0;
	datos->estadoMostrarSecuencia = 0;
	datos->estadoPerdio = 0;
	datos->ledmostrados = 0;
	datos->pasoJugador = 0;
	datos->ultimocolor = datos->secuencia[0];
}

int mostrarSecuencia(datosSimon *datos, int gano)
{
	int ret = 0;
	switch (datos->estadoMostrarSecuencia)
	{
	case 0:
		mostrarColor(APAGADO);
		datos->ledmostrados = 0;
		datos->espera = getTiempoColor(datos->contador, gano);
		(datos->estadoMostrarSecuencia)++;
		break;
	case 1:
		if (!(datos->espera))
		{
			(datos->estadoMostrarSecuencia)++;
		}
		break;
	case 2:
		datos->espera = getTiempoColor(datos->contador, gano);
		StartPWM(getFreqColor(datos->secuencia[datos->ledmostrados]));
		mostrarColor((coloresSimon) datos->secuencia[(datos->ledmostrados)++]);
		(datos->estadoMostrarSecuencia)++;
		break;
	case 3:
		if (!(datos->espera))
		{
			datos->espera = getTiempoEntreColores(datos->contador);
			StopPWM();
			mostrarColor(APAGADO);
			(datos->estadoMostrarSecuencia)++;
		}
		break;
	case 4:
		if (!datos->espera)
		{
			if (datos->ledmostrados <= datos->contador)
			{
				//muestro el que sigue
				datos->estadoMostrarSecuencia = 2;
			}
			else
			{
				//termine
				datos->estadoMostrarSecuencia = 0;
				datos->pasoJugador = 0;
				ret = 1;
			}
		}
		break;
	}
	return ret;
}

int hayboton(int *botones, int len)
{
	int i;
	int ret = 0;
	for (i = 0; i < len; i++)
	{
		if (botones[i])
		{
			ret++;
			break;
		}
	}
	return ret;
}

int getbotonactivo(int *botones, int len)
{
	int i;
	int ret = 0;
	for (i = 0; i < len; i++)
	{
		if (botones[i])
			ret += i;
	}
	return ret;
}

int esperarBotones(datosSimon *datos, int *subidas, int *bajadas)
{
	int i;
	int ret = 0;

	for (i = 0; i < NPULSADORES; i++)
	{
		if (subidas[i])
		{
			Board_LED_Set(2 + i, true);/*SonidoON(i);*/
			StartPWM(getFreqColor(i));
		}
		if (bajadas[i])
		{
			Board_LED_Set(2 + i, false);/*SonidoOFF(i);*/
			StopPWM();
		}
	}

	switch (datos->estadoEsperarBotones)
	{
	case 0:
		datos->espera = getTiempoEsperaBoton();
		(datos->estadoEsperarBotones)++;
		break;
	case 1:
		if (!datos->espera)
		{
			ret = PERDIO_SIMON;
		}
		else if (hayboton(bajadas, NPULSADORES))
		{
			datos->estadoEsperarBotones = 0;
			datos->ultimocolor = datos->secuencia[datos->pasoJugador];
			if (getbotonactivo(bajadas, NPULSADORES)
					== datos->secuencia[datos->pasoJugador])
			{
				(datos->pasoJugador)++;
				if (datos->pasoJugador > datos->contador)
				{
					(datos->contador)++;
					ret = GANO_SIMON;
				}
			}
			else
			{
				ret = PERDIO_SIMON;
			}
		}
		break;
	}
	return ret;
}

int mostrarPerdio(datosSimon *datos)
{
	int ret = 0;
	switch (datos->estadoPerdio)
	{
	case 0:
		datos->espera = getTiempoColor(datos->contador, 0);
		mostrarColor(APAGADO);
		(datos->estadoPerdio)++;
		break;
	case 1:
		if (!datos->espera)
		{
			StartPWM(S_PERDIO);
			mostrarColor((coloresSimon) datos->ultimocolor);
			datos->espera = getTiempoPerdio();
			(datos->estadoPerdio)++;
		}
		break;
	case 2:
		if (!datos->espera)
		{
			StopPWM();
			mostrarColor(APAGADO);
			ret++;
			(datos->estadoPerdio)++;
		}
	}
	return ret;
}

int mostrarGano(datosSimon *datos)
{
	int ret = 0;
	if (datos->estadoGano == 0)
	{
		datos->contador = 13;
		datos->secuencia[0] = ROJO;
		datos->secuencia[1] = AMARILLO;
		datos->secuencia[2] = AZUL;
		datos->secuencia[3] = VERDE;
		datos->secuencia[4] = ROJO;
		datos->secuencia[5] = AMARILLO;
		datos->secuencia[6] = AZUL;
		datos->secuencia[7] = VERDE;
		datos->secuencia[8] = ROJO;
		datos->secuencia[9] = AMARILLO;
		datos->secuencia[10] = AZUL;
		datos->secuencia[11] = VERDE;
		datos->secuencia[12] = ROJO;
		datos->secuencia[13] = AMARILLO;
		datos->secuencia[14] = AMARILLO;
		datos->secuencia[15] = AMARILLO;
		datos->estadoGano = 1;
	}
	if (datos->estadoGano == 1)
	{
		ret = mostrarSecuencia(datos, 1);
		if (ret)
		{
			datos->estadoGano = 0;
		}
	}
	return ret;
}

int jugarSimon(datosSimon *datos, int *subidas, int *bajadas)
{
	int ret = 1;
	int tmp;
	switch (datos->estadoJuego)
	{
	case 0:
		iniciarSimon(datos);
		(datos->estadoJuego)++;
		break;
	case 1:
		tmp = mostrarSecuencia(datos, 0);
		if (tmp)
			(datos->estadoJuego)++;
		break;
	case 2:
		tmp = esperarBotones(datos, subidas, bajadas);
		if (tmp == PERDIO_SIMON)
		{
			(datos->estadoJuego)++;
		}
		if (tmp == GANO_SIMON)
		{
			if (datos->contador == PASOS_SIMON)
			{
				datos->estadoJuego = 4;
			}
			else
			{
				datos->estadoJuego = 1;
			}
		}
		break;
	case 3:
		tmp = mostrarPerdio(datos);
		if (tmp)
		{
			datos->estadoJuego = 0;
			ret = 0;
		}
		break;
	case 4:
		tmp = mostrarGano(datos);
		if (tmp)
		{
			datos->estadoJuego = 0;
			ret = 0;
		}
		break;
	}
	return ret;
}

int xorshift(void)
{
	static int x = 0x12345678;
	static int y = 0x12345678;
	static int z = 0x12345678;
	static int w = 0x12345678;
	int tmp;
	tmp = (x ^ (x << 15));
	x = y;
	y = z;
	z = w;
	return w = (w ^ (w >> 21)) ^ (tmp ^ (tmp >> 4));
}

int generarRandom(void)
{
	return xorshift() & 0x03;
}
