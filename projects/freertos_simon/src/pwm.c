#include "board.h"

void InitPWM(void)
{
	Chip_SCU_PinMux(6, 12, MD_PUP | MD_EZI, FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 2, 8);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 2, 8);
	Chip_TIMER_Init(LPC_TIMER1);
	Chip_TIMER_PrescaleSet(LPC_TIMER1,
			Chip_Clock_GetRate(CLK_MX_TIMER1) / 1000000 - 1);

	/* Match 0 (period) */
	Chip_TIMER_MatchEnableInt(LPC_TIMER1, 0);
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER1, 0);
	Chip_TIMER_StopOnMatchDisable(LPC_TIMER1, 0);
	Chip_TIMER_SetMatch(LPC_TIMER1, 0, 100);

	/* Match 1 (duty) */
	Chip_TIMER_MatchEnableInt(LPC_TIMER1, 1);
	Chip_TIMER_ResetOnMatchDisable(LPC_TIMER1, 1);
	Chip_TIMER_StopOnMatchDisable(LPC_TIMER1, 1);
	Chip_TIMER_SetMatch(LPC_TIMER1, 1, 50);
	NVIC_EnableIRQ(TIMER1_IRQn);
}

void StartPWM(int frecuencia)
{
	uint32_t valor = (Chip_Clock_GetRate(CLK_MX_TIMER1)) / (frecuencia * 204);

	Chip_TIMER_Reset(LPC_TIMER1);
	Chip_TIMER_SetMatch(LPC_TIMER1, 0, valor);
	Chip_TIMER_SetMatch(LPC_TIMER1, 1, valor / 2);
	Chip_TIMER_Enable(LPC_TIMER1);
}

void StopPWM(void)
{
	Chip_TIMER_Disable(LPC_TIMER1);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 2, 8);
}

void TIMER1_IRQHandler(void)
{
	if (Chip_TIMER_MatchPending(LPC_TIMER1, 0))
	{
		Chip_TIMER_ClearMatch(LPC_TIMER1, 0);
		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 2, 8);
	}
	if (Chip_TIMER_MatchPending(LPC_TIMER1, 1))
	{
		Chip_TIMER_ClearMatch(LPC_TIMER1, 1);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 2, 8);
	}
}
