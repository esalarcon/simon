/*
 * debounce.h
 *
 *  Created on: 18 de jun. de 2016
 *      Author: juan
 */

#ifndef PROJECTS_FREERTOS_SIMON_INC_DEBOUNCE_H_
#define PROJECTS_FREERTOS_SIMON_INC_DEBOUNCE_H_

typedef enum
{
	ACTIVO_BAJO = 0, ACTIVO_ALTO
} debounce_estadoPin;

typedef struct
{
	int estado;
	int debtime;
	int flancoSubida;
	int flancoBajada;
	debounce_estadoPin PinActivo;
} debounce_data;

void debounce_init(debounce_data *estado, debounce_estadoPin estadoActivo);
int is_pin_activo(debounce_data *estado, int pin);

#endif /* PROJECTS_FREERTOS_SIMON_INC_DEBOUNCE_H_ */
