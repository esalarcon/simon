/*
 * pwm.h
 *
 *  Created on: 21 de jun. de 2016
 *      Author: juan
 */

#ifndef PROJECTS_FREERTOS_SIMON_INC_PWM_H_
#define PROJECTS_FREERTOS_SIMON_INC_PWM_H_

void InitPWM(void);
void StartPWM(int frecuencia);
void StopPWM(void);

#endif /* PROJECTS_FREERTOS_SIMON_INC_PWM_H_ */
