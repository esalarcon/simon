#ifndef PROJECTS_SIMON_INC_HWSIMON_H_
#define PROJECTS_SIMON_INC_HWSIMON_H_
#include "jugarsimon.h"

int getTiempoColor(int contador, int gano);
int getTiempoEntreColores(int contador);
int getTiempoEsperaBoton(void);
int getTiempoPerdio(void);
void mostrarColor(coloresSimon color);

#endif /* PROJECTS_SIMON_INC_HWSIMON_H_ */
