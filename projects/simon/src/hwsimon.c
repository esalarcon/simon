#include "board.h"
#include "jugarsimon.h"

int getTiempoColor(int contador, int gano)
{
	//Tomando una base de tiempo de 10ms.
	int ret;
	if (gano)
		return 10;
	if (contador < 5)
		ret = 40;
	if (contador >= 5 && contador < 13)
		ret = 30;
	if (contador >= 13)
		ret = 20;
	return ret;
}

int getTiempoEntreColores(int contador)
{
	//Tomando una base de tiempo de 10ms.
	return 5;
}

int getTiempoEsperaBoton(void)
{
	return 4500;
}

int getTiempoPerdio(void)
{
	return 150;
}

void mostrarColor(coloresSimon color)
{
	if (color == AZUL)
		Board_LED_Set(2, true);
	if (color == AMARILLO)
		Board_LED_Set(3, true);
	if (color == ROJO)
		Board_LED_Set(4, true);
	if (color == VERDE)
		Board_LED_Set(5, true);
	if (color == APAGADO)
	{
		Board_LED_Set(2, false);
		Board_LED_Set(3, false);
		Board_LED_Set(4, false);
		Board_LED_Set(5, false);
	}
}
