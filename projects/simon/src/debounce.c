#include "debounce.h"
#define DEBOUNCE_TIME	2

void debounce_init(debounce_data *estado, debounce_estadoPin estadoActivo)
{
	estado->debtime = DEBOUNCE_TIME;
	estado->PinActivo = estadoActivo;
	estado->estado = 0;
	estado->flancoBajada = 0;
	estado->flancoSubida = 0;
}

int is_pin_activo(debounce_data *estado, int pin)
{
	int ret = 0;
	estado->flancoBajada = 0;
	estado->flancoSubida = 0;

	switch (estado->estado)
	{
	case 0:
		if (pin == estado->PinActivo)
		{
			estado->debtime = DEBOUNCE_TIME;
			estado->estado = 1;
		}
		else
		{
			estado->estado = 0;
		}
		break;
	case 1:
		if (estado->debtime)
			return 0;
		if (pin == estado->PinActivo)
		{
			estado->estado = 2;
			estado->flancoSubida = 1;
			ret++;
		}
		else
		{
			estado->estado = 0;
		}
		break;
	case 2:
		if (pin != estado->PinActivo)
		{
			estado->debtime = DEBOUNCE_TIME;
			estado->estado = 3;
		}
		else
		{
			estado->estado = 2;
		}
		break;
	case 3:
		if (estado->debtime)
			return 0;
		if (pin != estado->PinActivo)
		{
			estado->estado = 0;
			estado->flancoBajada = 1;
		}
		else
		{
			estado->estado = 2;
		}
		break;
	}
	return ret;
}
