#include "main.h"
#include "board.h"
#include "debounce.h"
#include "pulsadores.h"
#include "pwm.h"
#include "jugarsimon.h"

int flag = 0;

static void initHardware(void)
{
	Board_Init();
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / 100);
	InitPulsadores();
	InitPWM();
}

void SysTick_Handler(void)
{
	flag = 1;
}

int main(void)
{
	int i;
	int jugando = 0;
	int iniciar = 0;
	datosSimon datosJuego;
	debounce_data debounce[NPULSADORES];
	int flancosSubidas[NPULSADORES];
	int flancosBajadas[NPULSADORES];

	flag = 0;
	initHardware();
	iniciarSimon(&datosJuego);
	for (i = 0; i < NPULSADORES; i++)
		debounce_init(&debounce[i], ACTIVO_BAJO);

	while (1)
	{
		if (flag)
		{
			flag = 0;
			iniciar = 0;
			for (i = 0; i < NPULSADORES; i++)
			{
				iniciar |= is_pin_activo(&debounce[i], LeerPulsador(i));
				flancosBajadas[i] = debounce[i].flancoBajada;
				flancosSubidas[i] = debounce[i].flancoSubida;
				debounce[i].debtime--;
			}
			generarRandomSimon();
			if (jugando || iniciar)
			{
				jugando = jugarSimon(&datosJuego, flancosSubidas,
						flancosBajadas);
			}
			datosJuego.espera--;
		}
	}
}
